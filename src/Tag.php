<?php

namespace CodeArtisan\LaravelTags;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name'];
}
