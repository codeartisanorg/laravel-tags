# Laravel Tags

This package adds tagging to your Laravel project.

It uses [polymorphic relations](https://laravel.com/docs/5.8/eloquent-relationships#polymorphic-relationships) which allows you to use it with any model as long as you include the `Taggable` trait.

## Installation

```
$ composer require codeartisan/laravel-tags
```

## Publish Migrations And Configurations

```
$ php artisan vendor:publish --tag codeartisan-tags
```

## How To Use It

### Enable Tags On A Model

```php
<?php
...
use CodeArtisan\LaravelTags\Taggable;
...
class BlogPost extends Model
{
    use Taggable;
    ...
}
```

### Create Tags

```php
Tag::create(['name' => 'New tag']);
```

### Attach Tags To A Model

```php
$post = BlogPost::find(1);
$post->syncTags(['foo', 'bar', 4, 5, 6]);
```

### Get Tags Linked To A Model

```php
$post = BlogPost::find(1);
dd($post->tags);
```

## Future Plans

* Sync tags via `tags` key when creating/updating a record
* Adding slugs
* Adding translations

## Changelog

**2019-08-22**

* Basic functionality to enable tagging
